/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sunjl
 */
public class HtmlCreate {

    private static String templetPATH = "./data/WebPage/templet/";
    private static String PATH = "./data/WebPage/sites/";

    private static String read() {
        File htmlFile = null;
        String strline = null;
        StringBuilder buffer = new StringBuilder();
        htmlFile = new File(templetPATH + "html.html");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(htmlFile));
            while ((strline = reader.readLine()) != null) {
                buffer.append(strline);
                buffer.append("\n");
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HtmlCreate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException e) {
            Logger.getLogger(HtmlCreate.class.getName()).log(Level.SEVERE, null, e);
        }
        return buffer.toString();
    }

    private static String write(String json, List<String> images, String title,List<String> styles) {
        File pagePath = new File(PATH + title);
        if (!pagePath.exists()) {
            pagePath.mkdirs();
        }
        File htmlFile = new File(pagePath + "/" + "index.html");
        try {
            if (!htmlFile.exists()) {
                htmlFile.createNewFile();
            }
            String htmlStr = read();
            FileWriter fileWriter = new FileWriter(htmlFile);
            String replaceAll = htmlStr.replaceAll("###title###", title).replaceAll("###slidejson###", json);
            fileWriter.write(replaceAll);
            fileWriter.close();
            copyFolder(new File(templetPATH + "js"), new File(PATH + "/" + title + "/js"));
            copyFolder(new File(templetPATH + "img"), new File(PATH + "/" + title + "/img"));
            for (String img : images) {
                copyFolder(new File(img), new File(PATH + "/" + title + "/img/" + img.substring(img.lastIndexOf("/"))));
            }
            copyFolder(new File(templetPATH + "images"), new File(PATH + "/" + title + "/images"));
            copyFolder(new File(templetPATH + "css"), new File(PATH + "/" + title + "/css"));
            File file=new File(PATH + "/" + title + "/css/SlideShowMakerStyle.css");
            fileWriter = new FileWriter(file,true);
            fileWriter.write("\r\n");
            fileWriter.write("#title{font-size:"+styles.get(0)+";\ncolor:"+styles.get(1)+";}\n");
            fileWriter.write("\r\n");
            fileWriter.write("#caption{font-size:"+styles.get(2)+";\ncolor:"+styles.get(3)+";}");
            fileWriter.close();
        } catch (IOException e) {
            Logger.getLogger(HtmlCreate.class.getName()).log(Level.SEVERE, null, e);
        }
        return htmlFile.getAbsolutePath();
    }

    /**
     * 复制一个目录及其子目录、文件到另外一个目录
     *
     * @param src
     * @param dest
     * @throws IOException
     */
    private static void copyFolder(File src, File dest) throws IOException {
        if (src.isDirectory()) {
            if (!dest.exists()) {
                dest.mkdir();
            }
            String files[] = src.list();
            for (String file : files) {
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                // 递归复制
                copyFolder(srcFile, destFile);
            }
        } else {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
            in.close();
            out.close();
        }
    }

    public static String initFile(String json, List<String> images, String title,List<String> styles) {
        return write(json, images, title,styles);
    }
}
