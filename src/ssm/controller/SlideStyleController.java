/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import static ssm.file.SlideShowFileManager.JSON_CAPTION;
import static ssm.file.SlideShowFileManager.JSON_IMAGE_FILE_NAME;
import static ssm.file.SlideShowFileManager.JSON_SLIDES;
import static ssm.file.SlideShowFileManager.JSON_TITLE;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.view.HtmlView;
import ssm.view.SlideShowMakerView;

/**
 *
 * @author sunjl
 */
public class SlideStyleController {

    @FXML
    private ComboBox  titlesizeBox;
    @FXML
    private ComboBox  titlecolorBox;
    @FXML
    private ComboBox  captionsizeBox;
    @FXML
    private ComboBox  captioncolorBox;
    @FXML
    private Button slide_ok;
    @FXML
    private Button slide_cancel;
    
    private Stage stage;
    
    private Stage stage2;

    SlideShowMakerView parentView;

    SlideShowModel slides;

    public void initSlideStyleController(Stage stage,Stage stage2) {
        this.stage=stage;
        this.stage2=stage2;
        titlesizeBox.setItems(FXCollections.observableArrayList("24px", "26px", "28px", "30px", "32px", "34px", "36px", "38px", "40px", "44px", "48px", "54px", "58px", "62px", "68px"));
        titlecolorBox.setItems(FXCollections.observableArrayList("black","yellow","red","blue","green","gray"));
        captionsizeBox.setItems(FXCollections.observableArrayList("10px","12px","14px","16px","18px","20px","22px","24px", "26px", "28px", "30px", "32px", "34px", "36px", "38px", "40px"));
        captioncolorBox.setItems(FXCollections.observableArrayList("black","yellow","red","blue","green","gray"));
        titlesizeBox.setValue("36px");
        titlecolorBox.setValue("black");
        captionsizeBox.setValue("14px");
        captioncolorBox.setValue("black");
    }

    @FXML
    protected void handleSubmitButtonAction() throws IOException, URISyntaxException {
        String title = slides.getTitle();
        List<Slide> slidelist = slides.getSlides();
        List<String> images = new ArrayList<String>();
        JsonArray jsons = makeSlidesJsonArray(slidelist);
        List<String> styles = new ArrayList<String>();
        JsonObject slideShowJsonObject = Json.createObjectBuilder()
                .add(JSON_TITLE, title)
                .add(JSON_SLIDES, jsons)
                .build();
        for (int i = 0; i < slidelist.size(); i++) {
            images.add(slidelist.get(i).getImagePath() + "/" + slidelist.get(i).getImageFileName());
        }
        styles.add(titlesizeBox.getValue().toString());
        styles.add(titlecolorBox.getValue().toString());
        styles.add(captionsizeBox.getValue().toString());
        styles.add(captioncolorBox.getValue().toString());
        String url=HtmlCreate.initFile(slideShowJsonObject.toString(), images, title,styles);
//        MsgDialog msgDialog =new MsgDialog();
//        msgDialog.showView(stage,"Export WebPage Is OK!");
        slide_cancel.getScene().getWindow().hide();
        new HtmlView().view(stage2,url.replaceAll("\\\\\\.", "").replaceAll("\\\\", "\\\\\\\\"));
    }

    @FXML
    protected void handleCancelButtonAction() {
        slide_cancel.getScene().getWindow().hide();
    }

    private JsonArray makeSlidesJsonArray(List<Slide> slides) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Slide slide : slides) {
            JsonObject jso = makeSlideJsonObject(slide);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject makeSlideJsonObject(Slide slide) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
                .add(JSON_CAPTION, slide.getCaption())
                .build();
        return jso;
    }

    public void setSlides(SlideShowModel slides) {
        this.slides = slides;
    }
}
