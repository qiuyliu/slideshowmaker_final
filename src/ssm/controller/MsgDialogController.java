/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ssm.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author sunjl
 */
public class MsgDialogController {
    @FXML
    private Label msg_label;
    @FXML
    private Button button_ok;
    
    @FXML
    protected void handleOk(){
        button_ok.getScene().getWindow().hide();
    }

    public void setLabel_msg(String msg) {
        msg_label.setText(msg);
    }
}
