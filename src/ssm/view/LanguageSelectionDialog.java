package ssm.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import static ssm.StartupConstants.ENGLISH_LANG;
import static ssm.StartupConstants.FINNISH_LANG;
import static ssm.StartupConstants.LABEL_LANGUAGE_SELECTION_PROMPT;
import static ssm.StartupConstants.OK_BUTTON_TEXT;
import static ssm.StartupConstants.STYLE_SHEET_UI;

/**
 *
 * @author McKillaGorilla
 */
public class LanguageSelectionDialog extends Stage {
    VBox vBox;
    Label languagePromptLabel;
    ComboBox languageComboBox;
    Button okButton;
    String selectedLanguage = ENGLISH_LANG;
    //String language = StartupConstants.UI_PROPERTIES_FILE_NAME_English;
    
    public LanguageSelectionDialog() {
	languagePromptLabel = new Label(LABEL_LANGUAGE_SELECTION_PROMPT);
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> languageChoices = FXCollections.observableArrayList();
	languageChoices.add(ENGLISH_LANG);
	languageChoices.add(FINNISH_LANG);
	languageComboBox = new ComboBox(languageChoices);
        /*if(languageComboBox.getItems().toString() == "English")
            selectedLanguage = StartupConstants.UI_PROPERTIES_FILE_NAME_English;
        else
            selectedLanguage = StartupConstants.UI_PROPERTIES_FILE_NAME_Finnish; */       
        
        languageComboBox.getSelectionModel().select(ENGLISH_LANG);
        languageComboBox.getSelectionModel().select(FINNISH_LANG);
        //languageComboBox.getStyleClass().add(CSS_CLASS_LANGUAGE_SELECTION_COMBO_BOX);

	okButton = new Button(OK_BUTTON_TEXT);
       // okButton.getStyleClass().add(CSS_CLASS_LANGUAGE_SELECTION_BUTTON);
	GridPane grid=new GridPane();
        grid.setHgap(1);
        grid.setVgap(1);
        grid.setAlignment(Pos.CENTER);
	vBox = new VBox();
        vBox.setSpacing(15);
        languagePromptLabel.setPrefHeight(35);
        languagePromptLabel.setFont(Font.font("Console", FontWeight.BOLD,14));
        languageComboBox.setPrefWidth(120);
        languageComboBox.setPrefHeight(35);
        languageComboBox.setPadding(new Insets(10));
        okButton.setPrefWidth(80);
        okButton.setPrefHeight(35);
	vBox.getChildren().add(languagePromptLabel);
	vBox.getChildren().add(languageComboBox);
	vBox.getChildren().add(okButton);
        grid.getChildren().add(vBox);
        //vBox.getStyleClass().add(CSS_CLASS_LANGUAGE_SELECTION_DIALOG);
	okButton.setOnAction(e -> {
	    selectedLanguage = languageComboBox.getSelectionModel().getSelectedItem().toString();
	    this.hide();
	});
        grid.setStyle(ENGLISH_LANG);
	//rgd(255, 180, 0)
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(grid);
        scene.getStylesheets().add(getClass().getResource("languageDialogStyle.css").toExternalForm());
        scene.getRoot().getStyleClass().add("language_selection_dialog");
        languageComboBox.getStyleClass().add("language_selection_combo_box");
        setScene(scene);
        scene.getStylesheets().add("path/vbox");
    }
    
    public String getSelectedLanguage() {
	return selectedLanguage;
    }
}
