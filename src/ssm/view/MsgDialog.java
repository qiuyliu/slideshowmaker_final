/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ssm.view;

import java.io.IOException;
import java.net.URL;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ssm.controller.MsgDialogController;

/**
 *
 * @author sunjl
 */
public class MsgDialog {

    public MsgDialog(){
        
    }
    public void showView(Stage stg,String msg) throws IOException {
        URL location = getClass().getResource("msgDialog.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        Parent root = fxmlLoader.load();
        ((MsgDialogController)fxmlLoader.getController()).setLabel_msg(msg);
        Scene scene = new Scene(root); 
        Stage stage=new Stage();
        stage.setTitle("Info");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(stg);
        stage.setScene(scene);
        stage.showAndWait();
    }
    
}
