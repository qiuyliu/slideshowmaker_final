/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.Color;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ModalDialog {

    Label label;
    Button btn;

    public ModalDialog(final Stage stg) {
        label=new Label();
        btn = new Button();

        final Stage stage = new Stage();
//Initialize the Stage with type of modal
        stage.initModality(Modality.APPLICATION_MODAL);
//Set the owner of the Stage 
        stage.initOwner(stg);
        stage.setTitle("Info");
        Group root = new Group();
        Scene scene = new Scene(root, 200, 120, Color.GRAY);

        btn.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                stage.hide();
            }
        });

        btn.setLayoutX(100);
        btn.setLayoutY(80);
        btn.setText("OK");
        
        label.setText("Export WebPage Is OK!");
        
        root.getChildren().add(label);
        root.getChildren().add(btn);
        stage.setScene(scene);
        stage.show();

    }

}
